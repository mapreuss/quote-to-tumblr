<?php $page = "home"; ?>
<?php include "inc/top.php" ?>
    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="img/Pencil.png" alt="">
                    <div class="intro-text">
                        <span class="name">Tumblr Quote Queuer</span>
                        <hr class="star-light">
                        <span class="skills">Queue your Kindle's quotes to Tumblr in seconds!</span>
                        <div class="col-lg-8 col-lg-offset-2 text-center">
                            <a href="auth.php" class="btn btn-lg btn-outline">
                                <i class="fa fa-tumblr-square"></i> &nbsp;Connect to Tumblr!
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Portfolio Grid Section -->
    <section id="what">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>What is it</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 text-center">
                    <img class="img-responsive center-block" src="img/what1.png" alt=""> <br>
                    <p>Everytime you select a quote on Kindle it adds this at a .txt file.</p>
                </div>
                <div class="col-sm-4 text-center">
                    <img class="img-responsive center-block" src="img/what2.png" alt=""> <br>
                    <p>If you do this for a while, there will be LOTS of quotes to post in Tumblr.</p>
                </div>
                <div class="col-sm-4 text-center">
                    <img class="img-responsive center-block" src="img/what3.png" alt=""> <br>
                    <p>So let us do it automatically for you!</p>
                </div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section class="success" id="how">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>How it Works</h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 text-center">
                    <img class="img-responsive center-block" src="img/how1.png" alt=""> <br>
                    <p>1. Login with your tumblr account.</p>
                </div>
                <div class="col-lg-3 text-center">
                    <img class="img-responsive center-block" src="img/how2.png" alt=""> <br>
                    <p>2. Select the blog where it gonna be posted.</p>
                </div>
                <div class="col-lg-3 text-center">
                    <img class="img-responsive center-block" src="img/how3.png" alt=""> <br>
                    <p>3. Write quote by quote or paste your .txt file.</p>
                </div>
                <div class="col-lg-3 text-center">
                    <img class="img-responsive center-block" src="img/how4.png" alt=""> <br>
                    <p>4. Ready to go! All your quotes are queued!</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Features</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 text-center">
                    <span class="fa-stack fa-lg">
                  
                    <i class="fa fa-lock fa-inverse"></i>
                      <i class="fa fa-circle fa-stack-2x"></i>
                    </span>
                    <p><strong>Safe</strong> I won't see your Tumblr login and password.</p>
                </div>
                <div class="col-lg-4 text-center">
                    
                    <span class="fa-stack fa-lg">
                    
                    <i class="fa fa-thumbs-up fa-inverse"></i>
                    <i class="fa fa-circle fa-stack-2x"></i>
                    </span>
                    <p><strong>Easy</strong> Just a few steps to do a lot of work.</p>
                </div>
                <div class="col-lg-4 text-center">
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-usd fa-stack-1x"></i>
                      <i class="fa fa-ban fa-stack-2x text-danger"></i>
                    </span>
                    <p><strong>Free and open-source</strong> No cost, no banners, no hidden words for now and ever!</p>
                </div>
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <a href="auth.php" class="btn btn-success btn-lg">
                        <i class="fa fa-tumblr-square"></i> &nbsp;Connect to Tumblr!
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Credits</h3>
                        <p>Tumblr Quote Queuer is an <a href="http://git.marta.preuss.nom.br/quote-to-tumblr">Open-source project</a> made with help and love by <a href="http://marta.preuss.nom.br">Marta Preuss</a>.</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Marta Preuss is everywhere:</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="http://facebook.com/mapreuss" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="http://twitter.com/suco_de_uva" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/profile/view?id=297414760&trk=nav_responsive_tab_profile_pic" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="http://git.marta.preuss.nom.br" class="btn-social btn-outline"><i class="fa fa-fw fa-git"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Wanna talk?</h3>
                        <p>If you have any problems or find any bugs please feel free to talk to me by social networks aside.</p>
                    </div>
                </div>
            </div>
        </div>
<?php include "inc/bot.php" ?>