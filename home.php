<?php
session_start();

require_once('tumblroauth/tumblroauth.php');
$consumer_key = "CdVSiQelILkuXpRF2dPi6TSBVAXs3XRyxpK8gYrnMXlREopcLY";
$consumer_secret = "8BY8nYfk3nuq4pB48IkQM7VQsH6U9w8kK4rtnnmev9vthVVUQO";
$tum_oauth = new TumblrOAuth($consumer_key, $consumer_secret, $_SESSION['request_token'], $_SESSION['request_token_secret']);
$access_token = $tum_oauth->getAccessToken($_REQUEST['oauth_verifier']);
unset($_SESSION['request_token']);
unset($_SESSION['request_token_secret']);
if (200 == $tum_oauth->http_code) {
} else {
  die('Unable to authenticate');
}
$tum_oauth = new TumblrOAuth($consumer_key, $consumer_secret, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$userinfo = $tum_oauth->get('http://api.tumblr.com/v2/user/info');
if (200 == $tum_oauth->http_code) {
} else {
  die('Unable to get info');
}
$_SESSION["access_token"] = $access_token['oauth_token'];
$_SESSION["access_token_secret"] = $access_token['oauth_token_secret'];

$page = "interna";
include "inc/top.php";
?>

 
 <section id="kindle">
<div class="container">
    <div class="row">
        <div class="col-lg-12">
        	<div class="text-center">
        	<br><br>
        		<h2>Post from Kindle</h2>
        		<hr class="star-primary">
        	</div>

			<form method="POST" action="api.php">
					<div class="row control-group">
		                <div class="form-group col-xs-12 floating-label-form-group controls floating-label-form-group-with-value">
		                    <label>Select the blog where it'll be posted:</label>
		                    <select name="blog" id="blog" class="form-control">
								<?php 
									$screen_name = $userinfo->response->user->name;
									for ($fln=0; $fln<count($userinfo->response->user->blogs); $fln=$fln+1) {
											echo("<option value='".($userinfo->response->user->blogs[$fln]->name)."'>".($userinfo->response->user->blogs[$fln]->title)."</option>");					
									}
								?>
							</select>	
		                    <p class="help-block text-danger"></p>
		                </div>
		            </div>

                    <div class="row control-group">
        	            <div class="form-group col-xs-6 floating-label-form-group controls">
        	                <label>Kindle's quotes</label>
        	                <textarea rows="5" class="form-control" placeholder="Kindle's quotes" id="textKindle" name="textKindle" required="" data-validation-required-message="Please enter a quote."></textarea>
        	                <p class="help-block text-danger"></p>
        	            </div>
        	            <div class="col-xs-6">
        	            	<h3>Post format</h3>
					<pre>
==========
Livro (Autor)
- Destaque Pos. 5.295 a 5.296  | Data de adição: sábado, 10 de maio de 2014 23h14min59s GMT-02:01

Frase
==========
Livro (Autor)
- Destaque Pos. 5.595  | Data de adição: sábado, 10 de maio de 2014 23h47min03s GMT-02:01

Frase
							</pre>
        	            </div>
                    </div>

					

					<input type="hidden" name="isKinde" value="true">

		            <div class="row control-group">
						<div class="form-group col-xs-12 text-center">
			                <button type="submit" class="btn btn-success btn-lg">Post</button>
			            </div>
					</div>
					
				</form>
		</div>
	</div>
</div>
</section>
<section class="success" id="quote">
<div class="container">
    <div class="row">
        <div class="col-lg-12">
        	<div class="text-center">
        	
        		<h2>Quote by quote</h2>
        		<hr class="star-light">
        	</div>
			
			<form method="POST" action="api.php">
			<input type="hidden" name="isKinde" value="false">
        	<div class="row control-group">
                <div class="form-group col-xs-12 floating-label-form-group controls floating-label-form-group-with-value">
                    <label>Select the blog where it'll be posted:</label>
                    <select name="blog" id="blog" class="form-control">
						<?php 
							$screen_name = $userinfo->response->user->name;
							for ($fln=0; $fln<count($userinfo->response->user->blogs); $fln=$fln+1) {
									echo("<option value='".($userinfo->response->user->blogs[$fln]->name)."'>".($userinfo->response->user->blogs[$fln]->title)."</option>");					
							}
						?>
					</select>	
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="row control-group">
	            <div class="form-group col-xs-12 floating-label-form-group controls">
	                <label>Quote</label>
	                <textarea rows="5" class="form-control" placeholder="Quote" id="text" name="text" required="" data-validation-required-message="Please enter a quote."></textarea>
	                <p class="help-block text-danger"></p>
	            </div>
            </div>
            <div class="row control-group">
	            <div class="form-group col-xs-12 floating-label-form-group controls floating-label-form-group-with-value">
	                <label>Author (Book)</label>
	                <input type="text" class="form-control" placeholder="Author (Book)" id="author" name="author" required="" data-validation-required-message="Please enter author's name." aria-invalid="false">
	                <p class="help-block text-danger"></p>
	            </div>
            </div>
            <div class="row control-group">
	            <div class="form-group col-xs-12 floating-label-form-group controls floating-label-form-group-with-value">
	                <label>Tags</label>
	                <input type="text" class="form-control" placeholder="Tags" id="postTags" name="postTags" required="" data-validation-required-message="Please enter author's name." aria-invalid="false">
	                <p class="help-block text-danger"></p>
	            </div>
            </div>
            <div class="row control-group">
				<div class="form-group col-xs-12 text-center">
	                <button type="submit" class="btn btn-outline btn-lg">Post</button>
	            </div>
			</div>
			</form>
		</div>
	</div>
</div>
</section>
<footer class="text-center">
<?php include "inc/bot.php" ?>