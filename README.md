# Tumblr Quote Queuer
---
Application which can queue a bunch of quotes on Tumblr. Ideal to whom have Kindle.

---
## How it works

I've made this because I have a Quote's Tumblr. But everytime I forgot to add a quote or another, my Kindle had collected a lot of them so I waste a lot of time queueing one by one.

So this app can read a Kindle-formated text, separate quote, author and book, make tags with author and book and queue these quotes at my tumblr of choice. Well, actually the user's tumblr of choice.

You can also input quote by quote manually, like tumblr itself, if you want to.

---
## How to install

You actually don't need to install, since you can run [the app right here](http://tudo.marta.preuss.nom.br/tumblr). But if you need the script for something, follow the steps:

1. Register a tumblr app to grab a key to development
2. clone this repository and run at a PHP enviroment
3. Change the keys at _auth.php_ and _api.php_
4. Enjoy.

---

## Credits
I did this myself, but with a lot of help of [Brunno Benatti](http://bitbucket.org/bbenatti), who did the parse, and [techslide sample](http://techslides.com/tumblr-api-example-using-oauth-and-php/), the only one which actually worked to tumblr connection.

---

## To-do
+ ~~Graphic interface~~
+ Save Tumblr session
+ Review text
+ Ajax feedback
+ Save preferences
+ .Txt uploader
+ Toggle between queue and post
+ List, Erase and Edit posts
+ OCR reader
+ Post other post types like figures and text
+ ~~Mobile interface~~
+ Translation